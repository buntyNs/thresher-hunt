$('#tool_table').DataTable({
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: [
        {extend: 'copy'},
        {extend: 'csv'},
        {extend: 'excel', title: 'ExampleFile'},
        {extend: 'pdf', title: 'ExampleFile'},

        {
            extend: 'print',
            customize: function (win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');

                $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
            }
        }
    ]

});

// Premade test data, you can also use your own
var DataUrl = "/agent/request/tool/"

$("#plants_select").change(function (event) {
    var plantId = event.target.value;
    $('.submit-request').attr('plantId_to', plantId);
    loadData(plantId);
});
$("#plants_select_admin").change(function (event) {
    var plantId = event.target.value;
    $('.submit-request').attr('plantId_to', plantId);
    DataUrl = "/admin/request/tool/";
    loadData(plantId);
});
$("#user_plant").change(function (event) {
    var plantId_from = event.target.value;
    $('.submit-request').attr('plantId_from', plantId_from);
});

function loadData(id) {
    console.log(id);
    $.ajax({
        type: 'GET',
        url: DataUrl + id,
        contentType: "text/plain",
        dataType: 'json',
        success: function (data) {
            JsonData = data;
            populateDataTable(JsonData);
        },
        error: function (e) {
            console.log(e);
            console.log("There was an error with your request...");
            console.log("error: " + JSON.stringify(e));
        }
    });
}

// populate the data table with JSON data
function populateDataTable(data) {
    console.log("populating data table...");
    // clear the table before populating it with more data
    $('#tool_table').dataTable().fnClearTable();
    var length = data.length;
    console.log("length",length);
    for (var i = 0; i < length; i++) {
        var tool = data[i];

        // You could also use an ajax property on the data table initialization
        $('#tool_table').dataTable().fnAddData([
            tool.inventory_code,
            tool.name,
            tool.model_no,
            tool.brand,
            tool.qty,
            tool.description,
            " <div class=\"btn-group\">\n" +
            " <button onclick='request(this)' qty = "+tool.qty+" toolId = " + tool.id + " data-toggle=\"modal\" data-target=\"#myModal6\" class=\"btn-white btn btn-xs request-button\">Request Tool</button>\n" +
            "</div>"

        ]);
    }
}


function request(data) {
    var toolId = data.getAttribute('toolId');
    var qty_ex = data.getAttribute('qty');
    console.log(toolId);
    $('.submit-request').attr('toolId', toolId);
    $('.submit-request').attr('qty_ex', qty_ex);
}

function submitRequest(data) {
    $("#plantId_from_error").text('');
    $("#qty_error").text('');
    var Requestdata = {
        toolId: data.getAttribute('toolId'),
        plantId_to: data.getAttribute('plantId_to'),
        plantId_from: data.getAttribute('plantId_from'),
        qty: $('#qty').val(),
        userId: data.getAttribute('userId'),
        exist_qty:data.getAttribute('qty_ex')
    }

    console.log(Requestdata);
    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }


    });

    $.ajax({

        type: 'POST',

        url: '/agent/request/tool',

        data: Requestdata,

        success: function (data) {
           if(data.errors){
               $.each(data.errors, function (key, val) {
                   $("#" + key + "_error").text(val[0]);
               });
           }else {
               window.location.replace("http://139.59.7.145:3000/agent/request/request_pending");
               // window.location.replace("http://localhost:8000/agent/request/request_pending");
           }
        }

    });
}

function refreshModel() {
    $("#qty").val("");
}

function handleRequest(data) {

    var req = {
        req_id :  data.getAttribute('req-id'),
        type : data.getAttribute('type'),
        page :data.getAttribute('page'),
    }
    console.log(req);

    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }


    });


    $.ajax({

        type: 'POST',

        url: '/agent/request/handle',

        data: req,

        success: function (data) {
            location.reload();


        }

    });

}

