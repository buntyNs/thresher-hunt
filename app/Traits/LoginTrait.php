<?php


namespace App\Traits;
use Validator;

trait LoginTrait
{
    public function validateInputRequest($request){

        $attributeNames = [
            'name' => "Name",
            'game' => "Game",
        ];

        $rules = [
            'name' => ['required','exists:users,user_name'],
            'game' => ['required','exists:games,name'],

        ];
        $messages = [
            'required' => ':attribute cannot be empty',
            'exists' => ':attribute not valid',
        ];


        $validator = Validator::make($request->all(),$rules,$messages);
        $validator->setAttributeNames($attributeNames);
        return $validator;

    }

    public function getIndexData($request){
        $data = $request->only('name','game');
        return $data;

    }

}
