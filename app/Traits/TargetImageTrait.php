<?php


namespace App\Traits;
use Validator;

trait TargetImageTrait
{
    public function validateInputRequest($request){
//        dd($request->game_id);

        $attributeNames = [
            'target_name' => "Target Name",
            'points' => "Points",
            'game_id' => "Game",
            'image' => "Image",
        ];

        $rules = [
            'target_name' => ['required'],
            'points' => ['required','integer'],
            'game_id' => ($request['game_id'] == "0" ?'required|integer':''),
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',

        ];
        $messages = [
            'required' => ':attribute cannot be empty',
        ];


        $validator = Validator::make($request->all(),$rules,$messages);
        $validator->setAttributeNames($attributeNames);
        return $validator;

    }

    public function getIndexData($request){
        $parameter = [];
        $parameter['target_name'] = $request->target_name;
        $parameter['points'] = $request->points;
        $parameter['game_id'] = $request->game_id;
        $parameter['image'] = $request->image;

        return $parameter;

    }

}
