<?php


namespace App\Traits;
use Validator;

trait HelloWorldTrait
{
    public function validateHelloRequest($request){

        $attributeNames = [
            'name' => "Name",
        ];

        $rules = [
            'name' => ['required'],

        ];
        $messages = [
            'required' => ':attribute cannot be empty',
        ];


        $validator = Validator::make($request->all(),$rules,$messages);
        $validator->setAttributeNames($attributeNames);
        return $validator;

    }

    public function getIndexData($request){
        $data = $request->only('name');
        return $data;

    }
}
