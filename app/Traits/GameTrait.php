<?php


namespace App\Traits;
use Validator;

trait GameTrait
{
    public function validateInputRequest($request){

        $attributeNames = [
            'name' => "Name",
            'time' => "Time",
        ];

        $rules = [
            'name' => ['required','unique:games'],
            'time' => ['required','integer'],

        ];
        $messages = [
            'required' => ':attribute cannot be empty',
        ];


        $validator = Validator::make($request->all(),$rules,$messages);
        $validator->setAttributeNames($attributeNames);
        return $validator;

    }

    public function getIndexData($request){
        $data = $request->only('name','time');
        return $data;

    }

    public function validatePointRequest($request){
        $attributeNames = [
            'user_id' => "User Id",
            'game_id' => "Game Id",
            'score' => "Score",
        ];

        $rules = [
            'user_id' => ['required','exists:users,id'],
            'game_id' => ['required','exists:games,id'],
            'score' => ['required','integer'],

        ];
        $messages = [
            'required' => ':attribute cannot be empty',
            'exists' => ':attribute not valid',
        ];


        $validator = Validator::make($request->all(),$rules,$messages);
        $validator->setAttributeNames($attributeNames);
        return $validator;
    }


    public function getPointData($request){
        $data = $request->only('game_id','user_id','score');
        return $data;

    }



}
