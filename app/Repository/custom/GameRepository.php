<?php


namespace App\Repository\custom;


use App\Repository\SuperRepository;

interface GameRepository extends SuperRepository
{
    public function updatePoints(array $data);
    public function getLeaderBoard($id);
}
