<?php


namespace App\Repository\custom\impl;


use App\Model\Game;
use App\Repository\custom\GameRepository;
use Illuminate\Support\Facades\DB;

class GameRepositoryImpl implements GameRepository
{
    private $game;

    public function __construct(Game $game)
    {
        $this->game = $game;
    }

    public function all()
    {
        return $this->game->all();
    }

    public function create(array $data)
    {

        return $this->game->create($data);
    }

    public function update(array $data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function show($id)
    {
        // TODO: Implement show() method.
    }

    public function updatePoints(array $data)
    {
        $update = DB::table('game_user')
            ->where('game_user.game_id',$data['game_id'])
            ->where('game_user.user_id',$data['user_id'])
            ->update(['game_user.score'=>$data['score']]);
        return $update;
    }

    public function getLeaderBoard($id)
    {
       return DB::table('game_user')
            ->join('users','users.id','game_user.user_id')
            ->where('game_user.game_id',$id)
            ->select('users.user_name','game_user.score')
            ->orderBy('game_user.score','DESC')
            ->get();
    }
}
