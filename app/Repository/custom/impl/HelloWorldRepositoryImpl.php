<?php


namespace App\Repository\custom\impl;


use App\Model\Hello;
use App\Repository\custom\HelloWorldRepository;
use App\Repository\SuperRepository;

class HelloWorldRepositoryImpl implements HelloWorldRepository
{

    private $hello;

    public function __construct(Hello $hello)
    {
        $this->hello = $hello;
    }

    public function all()
    {
        return $this->hello->all();
    }

    public function create(array $data)
    {
        // TODO: Implement create() method.
    }

    public function update(array $data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function show($id)
    {
        // TODO: Implement show() method.
    }
}
