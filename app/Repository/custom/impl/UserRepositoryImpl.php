<?php


namespace App\Repository\custom\impl;


use App\Repository\custom\UserRepository;
use Illuminate\Support\Facades\DB;

class UserRepositoryImpl implements UserRepository
{

    public function all()
    {
        // TODO: Implement all() method.
    }

    public function create(array $data)
    {
        // TODO: Implement create() method.
    }

    public function update(array $data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function show($id)
    {
        // TODO: Implement show() method.
    }

    public function getGameId(array $data)
    {
        $game = DB::table('game_user')->join('users','game_user.user_id','users.id')
            ->join('games','game_user.game_id','games.id')
            ->where('games.name',$data['game'])
            ->where('users.user_name',$data['name'])
            ->select('user_id','game_id','games.time','games.name as game','users.user_name')
            ->first();
        return $game;
    }
}
