<?php


namespace App\Repository\custom\impl;


use App\Model\TargetImage;
use App\Repository\custom\TargetImageRepository;

class TargetImageRepositoryImpl implements TargetImageRepository
{
    private $targetImage;

    public function __construct(TargetImage $targetImage)
    {
        $this->targetImage = $targetImage;
    }

    public function all()
    {
        return $this->targetImage->all();
    }

    public function create(array $data)
    {
        return $this->targetImage->create($data);
    }

    public function update(array $data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function show($id)
    {
        // TODO: Implement show() method.
    }
}
