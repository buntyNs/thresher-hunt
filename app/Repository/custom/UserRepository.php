<?php


namespace App\Repository\custom;


use App\Repository\SuperRepository;

interface UserRepository extends SuperRepository
{

    public function getGameId(array $data);
}
