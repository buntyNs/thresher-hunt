<?php

namespace App\Http\Controllers;

use App\Service\custom\LoginService;
use Illuminate\Http\Request;
use App\Traits\LoginTrait;

class LoginController extends Controller
{

    private $loginService;
    use LoginTrait;

    public function __construct(LoginService $loginService)
    {
        $this->loginService = $loginService;
    }


    public function login(Request $request){
        $validator = $this->validateInputRequest($request);

        if ($validator->fails()) {
            //return the errors to the previous page
            $errors = $validator->errors();
            return ["error"=>$errors,"code"=>422,"status"=>"error"];
        }

        //convert request to the data array
        $data = $this->getIndexData($request);
        //pass the data to the tool service
        return ["data"=>$this->loginService->login($data),"code"=>200,"status"=>"success"];

    }


}
