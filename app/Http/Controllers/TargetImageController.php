<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Service\custom\GameService;
use App\Service\custom\TargetImageService;
use App\Traits\TargetImageTrait;

class TargetImageController
{
    private $targetImageService;
    private $gameService;
    use TargetImageTrait;

    public function __construct(TargetImageService $targetImageService,GameService $gameService)
    {
        $this->targetImageService = $targetImageService;
        $this->gameService = $gameService;
    }

    public function imageView(){
        $targets = $this->targetImageService->all();
        return view('target_image.view',compact('targets'));
    }

    public function createView(){
        $games  = $this->gameService->all();
        return view('target_image.create',compact('games'));
    }

    public function uploadTarget(Request $request){
        $validator = $this->validateInputRequest($request);
        if ($validator->fails()) {
            //return the errors to the previous page
            return back()->withInput()->withErrors($validator);
        }
        //convert request to the data array
        $data = $this->getIndexData($request);

        //pass the data to the tool service
        $isCreated = $this->targetImageService->create($data);
        if ($isCreated) {
            $request->session()->flash('success', 'Game has been created');
            return redirect('/image');
        } else {
            $request->session()->flash('error', 'Please Try Again');
            return redirect('/image/add');
        }

    }

}
