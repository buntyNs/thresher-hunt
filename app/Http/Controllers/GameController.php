<?php

namespace App\Http\Controllers;

use App\Service\custom\GameService;
use Illuminate\Http\Request;
use App\Traits\GameTrait;
use phpDocumentor\Reflection\Element;

class GameController extends Controller
{

    private $gameService;
    use GameTrait;

    public function __construct(GameService $gameService)
    {
        $this->gameService = $gameService;
    }

    public function gameView(){
        $games = $this->gameService->all();
        return view('game.view',compact('games'));
    }

    public function createView(){
        return view('game.create');
    }

    public function createGame(Request $request){
        $validator = $this->validateInputRequest($request);
        if ($validator->fails()) {
            //return the errors to the previous page
            return back()->withInput()->withErrors($validator);
        }

        //convert request to the data array
        $data = $this->getIndexData($request);
        //pass the data to the tool service
        $isCreated = $this->gameService->create($data);
        if ($isCreated) {
            $request->session()->flash('success', 'Game has been created');
            return redirect('/game');
        } else {
            $request->session()->flash('error', 'Please Try Again');
            return back();
        }
    }

    public function updatePoints(Request $request){
        $validator = $this->validatePointRequest($request);

        if ($validator->fails()) {
            //return the errors to the previous page
            $errors = $validator->errors();

            return ["error"=>$errors,"code"=>422,"status"=>"error"];
        }

        //convert request to the data array
        $data = $this->getPointData($request);
        //pass the data to the tool service
        $update = $this->gameService->updatePoints($data);
        if($update){
            return ["code"=>201,"status"=>"success"];
        }else{
            return ["code"=>400,"status"=>"error"];
        }

    }


    public function getLeaderBoad(Request $request){

        return $this->gameService->getLeaderBoard($request['gameId']);

    }

}
