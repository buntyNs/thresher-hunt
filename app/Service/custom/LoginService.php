<?php


namespace App\Service\custom;


interface LoginService extends SuperService
{
    public function login(array $data);
}
