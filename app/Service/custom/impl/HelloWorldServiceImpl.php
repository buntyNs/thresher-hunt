<?php


namespace App\Service\custom\impl;


use App\Repository\custom\HelloWorldRepository;
use App\Service\custom\HelloWorldService;

class HelloWorldServiceImpl implements HelloWorldService
{
    private $helloRepository;

    public function __construct(HelloWorldRepository $helloRepository)
    {
        $this->helloRepository = $helloRepository;
    }


    public function all()
    {
        return $this->helloRepository->all();
    }

    public function create(array $data)
    {
        // TODO: Implement create() method.
    }

    public function update(array $data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function show($id)
    {
        // TODO: Implement show() method.
    }
}
