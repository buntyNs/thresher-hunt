<?php


namespace App\Service\custom\impl;


use App\Repository\custom\TargetImageRepository;
use App\Service\custom\TargetImageService;
use VWS;


class TargetImageServiceImpl implements TargetImageService
{
    private $targetImageRepository;
    public function __construct(TargetImageRepository $targetImageRepository)
    {
        $this->targetImageRepository = $targetImageRepository;
    }

    public function all()
    {
        return $this->targetImageRepository->all();
    }

    public  function create(array $data)
    {
        try {
            if (!empty($data['image']) && isset($data['image'])) {
                $uploadedFile = $data['image'];
                $fileName = time() . '.' . $uploadedFile->getClientOriginalExtension();
                $upload = $uploadedFile->move('img/tool', $fileName);
                if ($upload) {
                    $image_target['path'] = $fileName;
                    $image_target['name'] = $data['target_name'];
                    $image_target['points'] = $data['points'];
                    $image_target['game_id'] = $data['game_id'];
                    $res = VWS::addTarget(['name' => $data['target_name'], 'width' => 100, 'metadata'=> json_encode($image_target) , 'path' => public_path('/img/tool/'.$fileName)]);
                    if($res['status'] == 201){
                        $image_target['target_id'] = json_decode($res['body'])->target_id;
                        return $this->targetImageRepository->create($image_target);
                    }else{
                        return false;
                    }

                }
            }


        } catch (\Exception $e) {
            dd($e);
            $this->roolBack();
            return false;
        }

    }

    public function update(array $data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function show($id)
    {
        // TODO: Implement show() method.
    }
}
