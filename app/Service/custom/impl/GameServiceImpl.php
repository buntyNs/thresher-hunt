<?php


namespace App\Service\custom\impl;


use App\Model\Game;
use App\Repository\custom\GameRepository;
use App\Service\custom\GameService;

class GameServiceImpl implements GameService
{
    private $gameRepository;
    public function __construct(GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    public function all()
    {
        return $this->gameRepository->all();
    }

    public function create(array $data)
    {
        return $this->gameRepository->create($data);
    }

    public function update(array $data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function show($id)
    {
        // TODO: Implement show() method.
    }

    public function updatePoints(array $data)
    {
        return $this->gameRepository->updatePoints($data);
    }

    public function getLeaderBoard($id)
    {
        return $this->gameRepository->getLeaderBoard($id);
    }
}
