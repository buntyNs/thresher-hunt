<?php


namespace App\Service\custom\impl;


use App\Repository\custom\UserRepository;
use App\Service\custom\LoginService;

class LoginServiceImpl implements LoginService
{
    private $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function all()
    {
        // TODO: Implement all() method.
    }

    public function create(array $data)
    {
        // TODO: Implement create() method.
    }

    public function update(array $data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function show($id)
    {
        // TODO: Implement show() method.
    }

    public function login(array $data)
    {
       return $this->userRepository->getGameId($data);
    }
}
