<?php


namespace App\Service\custom;


interface GameService extends SuperService
{
    public function updatePoints(array $data);
    public function getLeaderBoard($id);
}
