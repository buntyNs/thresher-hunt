<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TargetImage extends Model
{
    protected $fillable = [
        'id', 'name' , 'path', 'points' ,'game_id','target_id'
    ];

    public function game(){
        return $this->belongsTo(Game::class);
    }
}
