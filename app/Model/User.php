<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $fillable = [
        'id', 'user_name'
    ];

    public function games(){
        return $this->belongsToMany(Game::class);
    }
}
