<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable = [
        'id', 'name' , 'time'
    ];

    public function users(){
        return $this->belongsToMany(User::class);
    }

    public function targetImages(){
        return $this->hasMany(TargetImage::class);
    }
}
