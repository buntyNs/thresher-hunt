<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Service\custom\HelloWorldService', 'App\Service\custom\impl\HelloWorldServiceImpl');
        $this->app->bind('App\Service\custom\GameService', 'App\Service\custom\impl\GameServiceImpl');
        $this->app->bind('App\Service\custom\UserService', 'App\Service\custom\impl\UserServiceImpl');
        $this->app->bind('App\Service\custom\TargetImageService', 'App\Service\custom\impl\TargetImageServiceImpl');
        $this->app->bind('App\Service\custom\LoginService', 'App\Service\custom\impl\LoginServiceImpl');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
