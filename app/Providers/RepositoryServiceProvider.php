<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repository\custom\HelloWorldRepository', 'App\Repository\custom\impl\HelloWorldRepositoryImpl');
        $this->app->bind('App\Repository\custom\GameRepository', 'App\Repository\custom\impl\GameRepositoryImpl');
        $this->app->bind('App\Repository\custom\UserRepository', 'App\Repository\custom\impl\UserRepositoryImpl');
        $this->app->bind('App\Repository\custom\TargetImageRepository', 'App\Repository\custom\impl\TargetImageRepositoryImpl');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
