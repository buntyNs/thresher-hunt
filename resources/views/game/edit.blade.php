@extends('master')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading" xmlns="http://www.w3.org/1999/html">
        <div class="col-sm-4">
            <h2>Add Plant</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/plant">Plant List</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Edit Plant</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        @if(session()->has('error'))
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{session()->get('error')}}
            </div>
        @endif
        {!! Form::open(['url'=>['/plant/edit',$plants->id],'files'=>'true','method'=>'POST']) !!}

        <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }}>
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $plants->name, ['class' => 'form-control', 'placeholder' => 'Plant Name'])}}
            <span class="text-danger">{{ $errors->has('name') ? $errors->first('name') : '' }}</span>
        </div>
        <a class="btn btn-white" href="/plant">Cancel</a>
        {{Form::submit('Save Changes', ['class'=>'btn btn-primary'])}}
        {!! Form::close() !!}
    </div>

@endsection
