@extends('master')

@section('content')

    <div class="row">
        <div class="col-lg-12">

            <div class="ibox product-detail">
                <div class="ibox-content">

                    <div class="row">
                        <div class="col-md-5">


                            <div class="product-images">

                                <div>
                                    <div class="image-imitation">
                                        <h3>{{$plants->name}}</h3>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="col-md-7">

                            <h2 class="font-bold m-b-xs">
                            {{$plants->status}}
                            </h2>

                            <hr>
                            <h4>Plant Name :  {{$plants->name}}</h4>

                            <div class="small text-muted">

                            </div>

                            <div class="text-right">
                                <div class="btn-group">
                                    <form method="GET" action="/plant" enctype="multipart/form-data">
                                    <button class="btn btn-white btn-sm"> Plant List </button>
                                    </form>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection
