@extends('master')

@section('css')

    <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
@endsection
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Game List</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Game List</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-8">
            <div class="title-action">
                <form method="GET" action="/game/add" enctype="multipart/form-data">
                    <button class="btn btn-primary">Add Game</button>
                </form>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        @if(session()->has('success'))
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{session()->get('success')}}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                <tr>
                                    <th>Game Id</th>
                                    <th>Game Name</th>
                                    <th data-hide="phone,tablet" class="text-right">Actions</th>
                                </tr>
                                </thead>
                                @foreach($games as $game)
                                    <tbody>
                                    <tr class="gradeX">
                                        <td>{{$game->id}}</td>
                                        <td>{{$game->name}}</td>
                                        <td class="text-right">
                                            <div class="btn-group">
                                                <form method="GET" action="#"
                                                      enctype="multipart/form-data">
                                                    <button class="btn-white btn btn-xs"
                                                            style="padding:7px 18px;font-size: 10px">Edit
                                                    </button>
                                                </form>
                                                <form method="GET" action="#" enctype="multipart/form-data">
                                                        <button class="btn-white btn btn-xs demo4"
                                                                style="padding:7px 18px;font-size: 10px">Delete
                                                        </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                @endforeach
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <script src="js/plugins/dataTables/datatables.min.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="js/plugins/sweetalert/sweetalert.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

    </script>



@endsection
