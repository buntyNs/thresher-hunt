@extends('master')
@section('css')
    <link href="/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
@endsection
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading" xmlns="http://www.w3.org/1999/html">
        <div class="col-sm-4">
            <h2>Request Tool</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Request</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight ">
        <div class="row">
            <div class="col-lg-12">

                    <select id="plants_select" class="form-control m-b" name="plants">
                        <option value='0'>-- Select Plants --</option>
                        @foreach($plants as $plant)
                            <option value={{$plant->id}}>{{$plant->name}}</option>
                        @endforeach
                    </select>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        @if(session()->has('msg'))
                            <div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                {{session()->get('msg')}}
                            </div>
                        @endif

                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">

                            <table class="table table-striped table-bordered table-hover dataTables-example" id="tool_table">
                                <thead>
                                <tr>
                                    <th>Inventory Code</th>
                                    <th>Tool Name</th>
                                    <th>Model No</th>
                                    <th>Brand</th>
                                    <th>Qty</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>


                                <tfoot>
                                <tr>
                                    <th>Inventory Code</th>
                                    <th>Tool Name</th>
                                    <th>Model No</th>
                                    <th>Brand</th>
                                    <th>Qty</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>

                            </tfoot>
                            </table>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="myModal6" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Select Quantity</h4>
                </div>

                <div class="modal-body">
                    <div class="form-group"><label>Select Your Plant : </label>
                        <select id="user_plant" class="form-control m-b" name="plants">
                            <option value='0'>-- Select Plants --</option>
                            @foreach($userPlant as $plant)
                                <option value={{$plant->id}}>{{$plant->name}}</option>
                            @endforeach
                        </select>
                        <span class="text-danger" id="plantId_from_error"></span>

                    </div>
                    <div class="form-group"><label>Qty : </label> <input id = "qty" type="number" placeholder="Quantity" class="form-control"></div>
                    <span class="text-danger" id="qty_error"></span>
                </div>
                <div class="modal-footer">
                    <button onclick="refreshModel()" type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button qty_ex = "" plantId_from = "0" userId = "{{Auth::user()->id}}"   type="button" toolId = "" plantId_to = "" class="btn btn-primary submit-request" onclick="submitRequest(this)">Submit</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/js/plugins/dataTables/datatables.min.js"></script>
    <script src="/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="/js/custom/request.js"></script>


@endsection
