<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>WKVGroup</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="/img/logo.png"/>


</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <img alt="image" class="rounded-circle" src="/img/logo.png"/>

            </div>
            <h3>Welcome to TRMS</h3>

            <p>Login in. To see it in action.</p>
            <form class="m-t" method = "post" action="/login">
            @csrf
                <div class="form-group">
                    <input name = "email" type="email" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <input name = "password" type="password" class="form-control" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                @if ($errors->has('message'))
                    <div class="error">{{ $errors->first('message') }}</div>
                @endif

            </form>

        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>

</body>

</html>
