@extends('master')

@section('content')

{{--    @include('layouts.page_title',['title' => 'Agent Dashboard', 'subTitle' => '','action'=>'','actionPath'=>""])--}}
<div class="row wrapper border-bottom white-bg page-heading" xmlns="http://www.w3.org/1999/html">
    <div class="col-sm-4">
        <h2>Plants</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">
                <a href="/"><strong>Home</strong></a>
            </li>

        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        @foreach($plants as $plant)
        <div class="col-lg-3">
            <div class="widget style1 navy-bg plant_widget" data-url ="{{env('APP_URL')}}/agent/plant/tool/{{$plant->plant_map_id}}">
                <div class="row">
                    <div class="col-4 text-center">
                        <i class="fa fa-home fa-5x"></i>
                    </div>
                    <div class="col-8 text-right">
                        <span>{{$plant->name}}</span>
{{--                        <h2 class="font-bold">30</h2>--}}
                    </div>
                </div>
            </div>
        </div>
        @endforeach

    </div>
</div>
@endsection
@section('script')
    <script src="/js/custom/dashboard.js"></script>
@endsection

