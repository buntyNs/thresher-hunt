@extends('master')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading" xmlns="http://www.w3.org/1999/html">
        <div class="col-sm-4">
            <h2>Add Target</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Add Target</strong>
                </li>
            </ol>
        </div>
        <div class="col-sm-8">
            <div class="title-action">

                <button class="btn btn-primary" type="button" onclick="goBack()">Go Back</button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        @if(session()->has('error'))
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{session()->get('error')}}
            </div>
        @endif
        {!! Form::open(['url'=>['/image/add'],'files'=>'true','method'=>'POST','enctype'=>'multipart/form-data']) !!}

        <div class="form-group" {{ $errors->has('target_name') ? 'has-error' : '' }}>
            {{Form::label('target_name', 'Target Name*')}}
            {{Form::text('target_name','', ['class' => 'form-control', 'placeholder' => 'Target Name'])}}
            <span
                class="text-danger">{{ $errors->has('target_name') ? $errors->first('target_name') : '' }}</span>
        </div>

        <div class="form-group" {{ $errors->has('points') ? 'has-error' : '' }}>
            {{Form::label('points', 'Points*')}}
            {{Form::text('points', '', ['class' => 'form-control', 'placeholder' => 'Points'])}}
            <span class="text-danger">{{ $errors->has('points') ? $errors->first('points') : '' }}</span>
        </div>
            <div class="form-group" {{ $errors->has('game_id') ? 'has-error' : '' }}>
                {{Form::label('game', 'Game*')}}
                <select id="game_id" class="form-control m-b" name="game_id">
                    <option value='0'>-- Select Game --</option>
                    @foreach($games as $game)
                        <option value={{$game->id}}>{{$game->name}}</option>
                    @endforeach
                </select>
                <span class="text-danger">{{ $errors->has('game_id') ? $errors->first('game_id') : '' }}</span>
            </div>

        <div class="form-group">
            <label>Image*</label>
            <input type="file" class="form-control border-input" name="image">
            @if ($errors->has('image'))
                <div class="error">{{ $errors->first('image') }}</div>
            @endif

        </div>

        <div class="form-group">
            <input type="hidden" value="create" name="method"/>
            <button class="btn btn-white" type="button" onclick="goBack()">Cancel</button>
            {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
        </div>


        {!! Form::close() !!}
    </div>
@endsection
@section('script')

    <script>
        function goBack() {
            window.history.back();
        }
    </script>

    <script>
        $(document).ready(function () {

            $(".btn-success").click(function () {
                var html = $(".clone").html();
                $(".increment").after(html);
            });

            $("body").on("click", ".btn-danger", function () {
                $(this).parents(".control-group").remove();
            });

        });

    </script>
@endsection
