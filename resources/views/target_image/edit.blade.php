@extends('master')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading" xmlns="http://www.w3.org/1999/html">
        <div class="col-sm-4">
            <h2>Edit Tool</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Edit Tool</strong>
                </li>
            </ol>
        </div>
        <div class="col-sm-8">
            <div class="title-action">
                <button class="btn btn-primary" type="button" onclick="goBack()">Go Back</button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        @if(session()->has('error'))
            <div role="alert" class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{session()->get('error')}}
            </div>
        @endif

        {!! Form::open(['url'=>['/plant/tool/edit',$tools[0]->id],'files'=>'true','method'=>'POST','enctype'=>'multipart/form-data']) !!}

        <div class="form-group" {{ $errors->has('inventory_code') ? 'has-error' : '' }}>
            {{Form::label('inventory_code', 'Inventory Code*')}}
            {{Form::text('inventory_code', $tools[0]->inventory_code, ['class' => 'form-control', 'placeholder' => 'Inventory Code','readonly' => 'true'])}}
            <span
                class="text-danger">{{ $errors->has('inventory_code') ? $errors->first('inventory_code') : '' }}</span>
        </div>

        <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }}>
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $tools[0]->name, ['class' => 'form-control', 'placeholder' => 'Tool Name'])}}
            <span class="text-danger">{{ $errors->has('name') ? $errors->first('name') : '' }}</span>
        </div>
        <div class="form-group" {{ $errors->has('brand') ? 'has-error' : '' }}>
            {{Form::label('brand', 'Brand Name')}}
            {{Form::text('brand', $tools[0]->brand, ['class' => 'form-control', 'placeholder' => 'Brand Name'])}}
            <span class="text-danger">{{ $errors->has('brand') ? $errors->first('brand') : '' }}</span>
        </div>

        <div class="form-group" {{ $errors->has('model_no') ? 'has-error' : '' }}>
            {{Form::label('model_no', 'Model No')}}
            {{Form::text('model_no',$tools[0]->model_no, ['class' => 'form-control', 'placeholder' => 'Model No'])}}
            <span class="text-danger">{{ $errors->has('model') ? $errors->first('model') : '' }}</span>
        </div>

        <div class="form-group" {{ $errors->has('qty') ? 'has-error' : '' }}>
            {{Form::label('qty', 'Quantity')}}
            {{Form::text('qty', $tools[0]->qty, ['class' => 'form-control', 'placeholder' => 'Quantity'])}}
            <span class="text-danger">{{ $errors->has('qty') ? $errors->first('qty') : '' }}</span>
        </div>

        <div class="form-group" {{ $errors->has('qty') ? 'has-error' : '' }}>
            {{Form::label('description', 'Description')}}
            {{Form::textarea('description', $tools[0]->description, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Description'])}}
            <span class="text-danger">{{ $errors->has('description') ? $errors->first('description') : '' }}</span>
        </div>

        <div class="form-group">
            <img src="/img/tool/{{$tools[0]->file_path}}" style="height: 300px;width: 300px">
        </div>
        <div class="form-group">
            <label>Image:</label>
            <input type="file" class="form-control border-input" name="image" multiple>
            <input type="hidden" class="form-control border-input" name="method" value="edit" multiple>
        </div>
        <div class="form-group">
            <button type="button" class="btn btn-white btn-sm" onclick="goBack()">Go Back</button>
            {{Form::submit('Update', ['class'=>'btn btn-primary btn-sm'])}}
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('script')
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
@endsection
