@extends('master')
@section('css')
    <link href="/css/plugins/slick/slick.css" rel="stylesheet">
    <link href="/css/plugins/slick/slick-theme.css" rel="stylesheet">
    @endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading" xmlns="http://www.w3.org/1999/html">
        <div class="col-sm-4">
            <h2>Edit Tool</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">Home</a>
                <li class="breadcrumb-item active">
                    <strong>Tool View</strong>
                </li>
            </ol>
        </div>
    </div>
     <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
    <div class="col-lg-12">

        <div class="ibox product-detail">
            <div class="ibox-content">

                <div class="row">

                    <div class="col-md-5">

                @if(empty($tool[0]->file_path))
                        <div class="product-images" >

                                <div>

                                    <div class="image-imitation">


                                    </div>
                                </div>



                        </div>


                        @else

                        <div class="product-images">
                            <div>

                                <div class="">
                                    <img src="/img/tool/{{$tool[0]->file_path}}" style="height: 300px;width: 300px">

                                </div>
                            </div>

                        </div>

                    </div>@endif

                    <div class="col-md-7">

                        <h2 class="font-bold m-b-xs">
                        {{$tool[0]->name}}
                        </h2>
                        <small>Brand: {{$tool[0]->brand}}</small><br>
                        <small>Model: {{$tool[0]->model_no}}</small><br>
                        <small>Inventory Code: {{$tool[0]->inventory_code}}</small><br>
                        <small>Quantity:{{$tool[0]->qty}}</small>

                        <hr>
                        <h4>Tool description</h4>

                        <div class="small text-muted">
                           {{$tool[0]->description}}
                        </div>

                        <div class="text-right">
                            <div class="btn-group">
                                    <button class="btn btn-primary" onclick="goBack()">Go Back</button>

                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>

    </div>
</div>


    @endsection
@section('script')
    <script src="/js/plugins/slick/slick.min.js"></script>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>

    <script>
        $(document).ready(function(){


            $('.product-images').slick({
                dots: true
            });

        });
    </script>
    @endsection
