@extends('master')

@section('css')
    <link href="/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Target List</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/">Home</a>
            </li>

            <li class="breadcrumb-item active">
                <strong>Target List</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">

            <form method="GET" action="/image/add" enctype="multipart/form-data">

                <button class="btn btn-primary" >Add Target</button>
            </form>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    @if(session()->has('success'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{session()->get('success')}}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">

                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Target Name</th>
                                <th>Game Name</th>
                                <th>Point</th>
                                <th>Target Id</th>
                                <th>Actions</th>
                            </tr>
                            </thead>


                            <tbody>
                            @if(!empty($targets))
                            @foreach($targets as $target)
                            <tr class="gradeX">
                                <td>{{$target->id}}</td>
                                <td>{{$target->name}}</td>
                                <td>{{$target->game->name}}</td>
                                <td>{{$target->points}}</td>
                                <td>{{$target->target_id}}</td>



                                <td class="text-right footable-visible footable-last-column">
                                    <div class="btn-group">
                                        <form method="GET" action="#" enctype="multipart/form-data">
                                            <button class="btn-white btn btn-xs">View</button>
                                        </form>
                                        <form method="GET" action="#" enctype="multipart/form-data">
                                            <button class="btn-white btn btn-xs">Edit</button>
                                        </form>
                                        <form method="POST" action="#" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            {{ method_field('POST') }}
                                            <button class="btn-white btn btn-xs" onclick="return confirm('Are you sure?')" >Delete</button>
                                        </form>

                                    </div>
                                </td>
                            </tr>
                                @endforeach
                                @endif
                            </tbody>


                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Target Name</th>
                            <th>Game Name</th>
                            <th>Point</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                    </table>

                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

@endsection




@section('script')
    <script src="/js/plugins/dataTables/datatables.min.js"></script>
    <script src="/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });


        </script>

@endsection

