@component('mail::message')
# Hi,
<p>Welcome to the Tools Registry Management System !</p>
<p>You have new request arrived from <strong>{{$plant_to}}</strong>.</p>
<p>Please be kind to get an action quickly.</p>
<table id="request">
    <thead>
    <tr>
        <th>Request Id</th>
        <th>Plant Name</th>
        <th>Tool Name</th>
        <th>Inventory Code</th>
        <th>Qty</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td> {{$req_id}} </td>
        <td> {{$plant_to}} </td>
        <td> {{$tool}} </td>
        <td> {{$inventory_code}} </td>
        <td> {{$qty}} </td>
        <td style="color: #1ab394"> Requested </td>
    </tr>
    </tbody>
</table>

<style>
    #request {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        font-size: smaller;
        width: 100%;
    }

    #request td, #request th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #request tr:nth-child(even){background-color: #1ab394}

    #request tr:hover {background-color: #ddd;}

    #request th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }
</style>
@component('mail::button', ['url' => 'http://139.59.7.145:3000'])
Login To TRMS
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
