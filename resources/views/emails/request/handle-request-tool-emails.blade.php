@component('mail::message')

# Hi,
<p>Welcome to the Tools Registry Management System !</p>
@if($status == 'Accepted')
<p><strong>{{$plant_to}}</strong> has been approved your request.</p>
@endif
@if($status == 'Received')
<p><strong>{{$plant_from}}</strong> has been received your tool.</p>
@endif
@if($status == 'Canceled')
<p><strong>{{$plant_to}}</strong> has been Canceled tool request.</p>
@endif

<table id="request">
    <thead>
    <tr>
        <th>Request Id</th>
        <th>Plant Name</th>
        <th>Tool Name</th>
        <th>Inventory Code</th>
        <th>Qty</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td> {{$req_id}} </td>
        @if($status == 'Accepted')
            <td> {{$plant_to}} </td>
        @endif
        @if($status == 'Received')
            <td> {{$plant_from}} </td>
        @endif
        @if($status == 'Canceled')
            <td> {{$plant_to}} </td>
        @endif
        <td> {{$tool}} </td>
        <td> {{$inventory_code}} </td>
        <td> {{$qty}} </td>
        <td style="color: #1ab394">{{$status}}</td>
    </tr>
    </tbody>
</table>

<style>
    #request {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        font-size: smaller;
        border-collapse: collapse;
        width: 100%;
    }

    #request td, #request th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #request tr:nth-child(even){background-color: #1ab394}

    #request tr:hover {background-color: #ddd;}

    #request th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }
</style>

@component('mail::button', ['url' => 'http://139.59.7.145:3000'])
Login To TRMS
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
