@component('mail::message')
# Hi {{ $fullname }},
<p>Welcome to the Tools Registry Management System !</p>
<p>Now you can login to system by below credentials.</p>

<p><strong>Username</strong> : {{$username}}</p>
<p><strong>Password</strong> : {{$password}}</p>

@component('mail::button', ['url' => 'http://139.59.7.145:3000'])
Login To TRMS
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
