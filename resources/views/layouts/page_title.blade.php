<div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                    <h2>{{$title}}</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">Home</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>{{$subTitle}}</strong>
                        </li>
                    </ol>
            </div>
            @if($actionPath != "")
            <div class="col-sm-8">
                    <div class="title-action">
                        <a href="{{$actionPath}}" class="btn btn-primary">{{$action}}</a>
                    </div>
            </div>
            @else
            <div></div>
           
            @endif
    </div>
