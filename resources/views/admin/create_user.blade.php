@extends('master')

@section('css')
    <link href="/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
@endsection
@section('content')
    @include('layouts.page_title',['title' => 'Create New User', 'subTitle' => 'create_user','action'=>'View Users','actionPath'=>""])
    <div class="wrapper wrapper-content animated fadeInRight">
        @if (Session::has('error'))
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{session()->get('error')}}
            </div>
        @endif
        <div class="row">

            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-content">
                        <form method="POST" action="/user/create" id="user_data">
                            @csrf
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">First Name</label>
                                <div class="col-sm-10">
                                    <input name="first_name" type="text" class="form-control"
                                           value="{{old('first_name')}}">
                                    @if ($errors->has('first_name'))
                                        <div class="error">{{ $errors->first('first_name') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Last Name</label>
                                <div class="col-sm-10"><input name="last_name" type="text" class="form-control"
                                                              value="{{old('last_name')}}">
                                    @if ($errors->has('last_name'))
                                        <div class="error">{{ $errors->first('last_name') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group  row">
                                <label class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input name="email" type="email" class="form-control" value="{{old('email')}}">
                                    @if ($errors->has('email'))
                                        <div class="error">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Contact No</label>
                                <div class="col-sm-10"><input name="contact" type="text" class="form-control"
                                                              value="{{old('contact')}}">
                                    @if ($errors->has('contact'))
                                        <div class="error">{{ $errors->first('contact') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row"><label class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10"><input type="password" class="form-control" name="password" autocomplete=""
                                                              value="{{old('password')}}">
                                    @if ($errors->has('password'))
                                        <div class="error">{{ $errors->first('password') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="hr-line-dashed">
                            </div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Plants</label>
                                <div class="col-sm-10">
                                    @if ($errors->has('plant_array'))
                                        <div class="error">Please select at least one plant for user</div>
                                    @endif
                                    <div id="form" action="create_user" class="wizard-big">
                                        <select id="box2View" name="plant" class="form-control dual_select" multiple>
                                            @foreach($option as $options)
                                                <option  plantId={{$options->id}}>{{$options->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <input type="hidden" name="plant_array" value="null" id="setState">
                            <input type="hidden" name="admin" value="agent" >
                            <input type="hidden" name="method" value="create">
                            <div class="hr-line-dashed"></div>
                            <div class="form-group row">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a class="btn btn-white btn-sm" href="/user">Cancel</a>
                                    <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- Dual Listbox -->
    <script src="/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    <script>
        $('.dual_select').bootstrapDualListbox({
            selectorMinimalHeight: 160
        });
        $('#user_data').submit(function (event) {
//                event.preventDefault();
            var items = $("select#bootstrap-duallistbox-selected-list_plant option");
            var plant_array = [];
            var i;
            for (i = 0; i < items.length; i++) {
                console.log(items[i].getAttribute('plantId'));
                plant_array.push(items[i].getAttribute('plantId'));
            }
            $('#setState').val(plant_array);

        });
    </script>
@endsection
