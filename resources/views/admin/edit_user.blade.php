@extends('master')


@section('css')
    <link href="css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
@endsection
@section('content')



    <div class="row wrapper border-bottom white-bg page-heading" xmlns="http://www.w3.org/1999/html">
        <div class="col-sm-4">
            <h2>Edit Tool</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/user">User List</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Edit User</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        @if (Session::has('error'))
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{session()->get('error')}}
            </div>
        @endif
        <div class="row">

            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-content">

                        <form method="POST" action="/user/edit/{{$user->id}}" id="user_data">

                            @csrf
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">First Name</label>
                                <div class="col-sm-10">
                                    <input name="first_name" type="text" class="form-control"
                                           value="{{($user->first_name)}}">
                                    @if ($errors->has('first_name'))
                                        <div class="error">{{ $errors->first('first_name') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Last Name</label>
                                <div class="col-sm-10"><input name="last_name" type="text" class="form-control"
                                                              value="{{$user->last_name}}">
                                    @if ($errors->has('last_name'))
                                        <div class="error">{{ $errors->first('last_name') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10"><input name="email" type="email" class="form-control"
                                                              value="{{$user->email}}">
                                    @if ($errors->has('email'))
                                        <div class="error">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Contact No</label>
                                <div class="col-sm-10"><input name="contact" type="text" class="form-control"
                                                              value="0{{$user->contact}}">
                                    @if ($errors->has('contact'))
                                        <div class="error">{{ $errors->first('contact') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="hr-line-dashed">
                            </div>
                            <input type="hidden" name="plant_array" value="null" id="setState">
                            <input type="hidden" name="method" value="edit" id="setState">

                            <div class="form-group row">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-white btn-sm" type="submit">Cancel</button>
                                    <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
   <script>

    </script>
@endsection
