@extends('master')

@section('css')

    <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
@endsection
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>User List</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>User List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-8">
        <div class="title-action">
            <form method="GET" action="/user/create" enctype="multipart/form-data">
                <button class="btn btn-primary" >Add User</button>
            </form>
        </div>
    </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        @if (Session::has('success'))
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{session()->get('success')}}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        @if(session()->has('msg'))
                            <div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                {{session()->get('msg')}}
                            </div>
                        @endif
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>

                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Status</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                                </thead>
                                @foreach($allUser as $user)
                                <tbody>
                                <tr class="gradeX">
                                    <td>{{$user->first_name}}</td>
                                    <td>{{$user->last_name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td class="center">{{$user->contact}}</td>
                                    @if($user->status == 1)
                                    <td>Available</td>
                                        @else
                                        <td>Disabled</td>
                                    @endif
                                    <td class="text-right">
                                        <div class="btn-group" >
                                            <form method="GET" action="/user/password_change/{{$user->id}}" enctype="multipart/form-data">
                                                @if($user->status==1)
                                                    <button class="btn-white btn btn-xs" style="padding:7px 18px;font-size: 10px">Password Change</button>
                                                @else
                                                    <button class="btn-white btn btn-xs" style="padding:7px 18px;font-size: 10px" disabled>Password Change</button>
                                                @endif
                                            </form>
                                            <form method="GET" action="/user/show/{{$user->id}}" enctype="multipart/form-data">
                                                @if($user->status==1)
                                                    <button class="btn-white btn btn-xs" style="padding:7px 18px;font-size: 10px">View</button>
                                                @else
                                                    <button class="btn-white btn btn-xs" style="padding:7px 18px;font-size: 10px" disabled>View</button>
                                                @endif
                                            </form>
                                            <form method="GET" action="/user/edit/{{$user->id}}" enctype="multipart/form-data">
                                                @if($user->status==1)
                                                    <button class="btn-white btn btn-xs" style="padding:7px 18px;font-size: 10px">Edit</button>
                                                @else
                                                    <button class="btn-white btn btn-xs" style="padding:7px 18px;font-size: 10px" disabled>Edit</button>
                                                @endif
                                            </form>
                                            <form method="POST" action="/user/delete/{{$user->id}}" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                {{ method_field('POST') }}
                                                @if($user->status==0)
                                                    <button class="btn-white btn btn-xs demo4"  disabled style="padding:7px 18px;font-size: 10px">Delete</button>
                                                @else
                                                    <button class="btn-white btn btn-xs demo4" onclick="return confirm('Are you sure?')" style="padding:7px 18px;font-size: 10px">Delete</button>
                                                @endif

                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                                @endforeach
                                <tfoot>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <script src="js/plugins/dataTables/datatables.min.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

    </script>

@endsection
