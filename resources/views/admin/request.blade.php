@extends('master')

@section('css')
    <link href="/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Request List</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="">Home</a>
                </li>


                <li class="breadcrumb-item active">
                    <strong>Request List</strong>
                </li>
            </ol>
        </div>
        <div class="col-sm-8">
            <div class="title-action">

                {{--                <form method="GET" action="/plant/tool/create/{{$plant_map_id}}" enctype="multipart/form-data">--}}

{{--                <button class="btn btn-primary" >Add Tool</button>--}}
                </form>
            </div>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        @if(session()->has('msg'))
                            <div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                {{session()->get('msg')}}
                            </div>
                        @endif

                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">

                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>Request Id</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Tool Name</th>
                                    <th>Inventory Code</th>
                                    <th>Qty</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                </tr>
                                </thead>


                                <tbody>

                                @foreach($reqs as $req)
                                    <tr class="gradeX">
                                        <td>{{$req->request_id}}</td>
                                        <td>{{$req->from}}</td>
                                        <td>{{$req->to}}</td>
                                        <td>{{$req->tool_name}}</td>
                                        <td>{{$req->inventory_code}}</td>
                                        <td>{{$req->qty}}</td>
                                        <td>{{$req->created_at}}</td>
                                        <td>
                                            @if($req->status == "Request")
                                                <span class="label label-primary">{{$req->status}}</span>
                                            @elseif($req->status == "Accept")
                                                <span class="label label-info">{{$req->status}}</span>
                                            @elseif($req->status == "Cancel")
                                                <span class="label label-danger">{{$req->status}}</span>
                                            @elseif($req->status == "Received")
                                                <span class="label label-success">{{$req->status}}</span>
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>


                                <tfoot>
                                <tr>
                                    <th>Request Id</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Tool Name</th>
                                    <th>Inventory Code</th>
                                    <th>Qty</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                </tr>
                                </tfoot>
                            </table>

                            </tfoot>
                            </table>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection




@section('script')
    <script src="/js/plugins/dataTables/datatables.min.js"></script>
    <script src="/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="/js/custom/request.js"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });


    </script>

@endsection

