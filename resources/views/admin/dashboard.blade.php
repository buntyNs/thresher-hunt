@extends('master')

@section('content')


    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Admin Dashboard</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="">Home</a>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
    {{--@foreach($dashboard as $dash)--}}
        <div class="row">
            <div class="col-lg-3">
                <div class="widget style1 navy-bg">
                    <a href="/plant" style="color: white">
                    <div class="row">
                        <div class="col-4 text-center">
                            <i class="fa fa-home fa-5x"></i>
                        </div>
                        <div class="col-8 text-right">
                            <span>Plants</span>
{{--                            <h2 class="font-bold">{{$dashboard['plants']}}</h2>--}}
                            <h2 class="font-bold">TEST</h2>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget style1 navy-bg">
                    <a href="/tool" style="color: white">
                    <div class="row">
                        <div class="col-4">
                            <i class="fa fa-wrench fa-5x"></i>
                        </div>
                        <div class="col-8 text-right">
                            <span> Tools</span>
{{--                            <h2 class="font-bold">{{$dashboard['tools']}}</h2>--}}
                            <h2 class="font-bold">TEST</h2>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget style1 navy-bg">
                    <a href="/request" style="color: white">
                    <div class="row">
                        <div class="col-4">
                            <i class="fa fa-envelope-o fa-5x"></i>
                        </div>
                        <div class="col-8 text-right">
                            <span>Request</span>
{{--                            <h2 class="font-bold">{{$dashboard['requests']}}</h2>--}}
                            <h2 class="font-bold">TEST</h2>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget style1 navy-bg">
                    <a href="/user" style="color: white">
                    <div class="row">
                        <div class="col-4">
                            <i class="fa fa-users fa-5x"></i>
                        </div>
                        <div class="col-8 text-right">
                            <span>Users</span>
{{--                            <h2 class="font-bold">{{$dashboard['users']}}</h2>--}}
                            <h2 class="font-bold">TEST</h2>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
        </div>
        {{--@endforeach--}}
    </div>


@endsection
