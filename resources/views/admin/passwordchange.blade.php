@extends('master')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading" xmlns="http://www.w3.org/1999/html">
        <div class="col-sm-4">
            <h2>Edit Tool</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/user">Users List</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Password Change</strong>
                </li>
            </ol>
        </div>
    </div>



    <div class="wrapper wrapper-content animated fadeInRight">
        @if (Session::has('error'))
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{session()->get('error')}}
            </div>
        @endif
        <div class="row">

            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-content">

                        <form method="POST" action="/user/password_change/{{$user_id}}" id="user_data">

                            @csrf
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Current Password</label>
                                <div class="col-sm-10">
                                    <input name="current_password" type="password" class="form-control"
                                           value="">
                                    @if ($errors->has('current_password'))
                                        <div class="error">{{ $errors->first('current_password') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group  row"><label class="col-sm-2 col-form-label">New Password</label>
                                <div class="col-sm-10"><input name="password" type="password" class="form-control"
                                                              value="">
                                    @if ($errors->has('password'))
                                        <div class="error">{{ $errors->first('password') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Re-enter Password</label>
                                <div class="col-sm-10"><input name="password_confirmation" type="password" class="form-control"
                                                              value="">
                                    @if ($errors->has('password_confirmation'))
                                        <div class="error">{{ $errors->first('password_confirmation') }}</div>
                                    @endif
                                </div>
                            </div>



                            <div class="hr-line-dashed">
                            </div>

                            <input type="hidden" name="role" value="agent" id="setState">

                            <div class="form-group row">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a class="btn btn-white btn-sm" href="/user">Cancel</a>
                                    <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
