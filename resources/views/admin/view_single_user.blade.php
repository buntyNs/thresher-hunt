@extends('master')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading" xmlns="http://www.w3.org/1999/html">
        <div class="col-sm-4">
            <h2>User</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/user">Users List</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>User view</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">

                <div class="ibox product-detail">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-md-5">


                                <div class="product-images">

                                    <div>
                                        <div class="image-imitation">
                                            <h3>{{$users->first_name}}</h3>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="col-md-7">

                                <h2 class="font-bold m-b-xs">
                                    {{$users->first_name}} {{$users->last_name}}
                                </h2>
                                <small>{{$users->contact}} Contact .</small>
                                <hr>
                                <h4>

                                </h4>

                                <div class="small text-muted">
                                    <h4>First Name: {{$users->first_name}}</h4>
                                    <h4>Last Name: {{$users->last_name}}</h4>
                                    <h4>Contact: {{$users->contact}}</h4>
                                    <h4>Email: {{$users->email}}</h4>
                                    <h4>Role: {{$users->role}}</h4>

                                </div>

                                <div class="text-right">
                                    <div class="btn-group">
                                        <a class="btn btn-white btn-sm" href="/user">User List
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>


@endsection
