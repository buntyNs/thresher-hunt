<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>TRMS</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/css/plugins/summernote/summernote-bs4.css" rel="stylesheet">
    <link href="/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    @yield('css')

</head>

<body class="">

    <div id="wrapper">
<!-- sidebar menu -->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <img alt="image" style="width: 40px;" class="rounded-circle" src="/img/user.png"/>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
{{--                            <span class="block m-t-xs font-bold">{{Auth::user()->first_name . " ".  Auth::user()->last_name}}</span>--}}

                        </a>

                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>

{{--                @if (Session::has('menu'))--}}
{{--                    @foreach(Session::get('menu') as $menu)--}}
{{--                        <li>--}}
{{--                            <a href="{{$menu->route}}"><i class="{{$menu->icon}}"></i> <span class="nav-label">{{$menu->name}}</span></a>--}}
{{--                        </li>--}}
{{--                    @endforeach--}}
{{--                @endif--}}

            </ul>
        </div>
    </nav>
<!-- sidebar menu wnd -->


        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
<!-- header -->
        <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">



                <li>
                    <a href="/logout">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
<!-- headr end -->

            </div>
           @yield('content')

            <!-- footer -->
            <div class="footer">
                <div>
                    <strong>Copyright</strong> Axiata Digital Labs &copy; 2014-2018
                </div>
            </div>
            <!-- footer end -->

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="/js/plugins/footable/footable.all.min.js"></script>


    <!-- Custom and plugin javascript -->
    <script src="/js/inspinia.js"></script>
    <script src="/js/plugins/pace/pace.min.js"></script>
    <script src="/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/js/custom/aleart.js"></script>

    @yield('script')

</body>

</html>
