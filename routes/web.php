<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.dashboard');
});

Route::get('/hello', 'HelloWorldController@view');


//game
Route::get('/game', 'GameController@gameView');
Route::get('/game/add', 'GameController@createView');
Route::post('/game/add', 'GameController@createGame');
Route::get('/game/edit', 'GameController@editView');
Route::post('/game/edit', 'GameController@editGame');
Route::post('/game/delete', 'GameController@gameView');

//target image
Route::get('/image', 'TargetImageController@imageView');
Route::get('/image/add', 'TargetImageController@createView');
Route::post('/image/add', 'TargetImageController@uploadTarget');
Route::get('/image/edit', 'TargetImageController@editView');
Route::post('/image/edit', 'TargetImageController@editImage');
Route::post('/image/delete', 'TargetImageController@gameView');


